import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Wellcome from "../screens/Wellcome";
import Login from "../screens/Login";
import SignUp from "../screens/SignUp";
import Browse from "../screens/Browse";
import Settings from "../screens/Settings";

const Stack = createStackNavigator();

export default function StackNavigation() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackImage: () => (
          <Image source={require("../assets/icons/back.png")} />
        ),
        headerStyle: {
          borderBottomColor: "transparent",
          backgroundColor: "transparent",
          elevation: 0,
        },
      }}
    >
      <Stack.Screen
        name="welcome"
        component={Wellcome}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="login" component={Login} options={{ title: "" }} />
      <Stack.Screen name="signup" component={SignUp} options={{ title: "" }} />
      <Stack.Screen name="browse" component={Browse} options={{ title: "" }} />
      <Stack.Screen
        name="settings"
        component={Settings}
        options={{ title: "" }}
      />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({});
