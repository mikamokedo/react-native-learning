import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons";
import Wellcome from "../screens/Wellcome";
import Browse from "../screens/Browse";
import SettingsTab from "../screens/SettingsTab";
import StackNavigation from "./StackNavigation";
const Tab = createBottomTabNavigator();
export default function TabNavigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === "wellcome") {
              iconName = focused ? "ios-home" : "ios-home";
            } else if (route.name === "settings") {
              iconName = focused ? "ios-list-box" : "ios-list";
            } else {
              iconName = focused ? "logo-chrome" : "logo-chrome";
            }

            // You can return any component that you like here!
            return (
              <Ionicons name={iconName as string} size={size} color={color} />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: "#1e88e5",
          inactiveTintColor: "gray",
        }}
      >
        <Tab.Screen name="wellcome" component={StackNavigation} />
        <Tab.Screen name="settings" component={SettingsTab} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({});
