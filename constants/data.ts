export const categories = [
        {
            id: 'plants',
            name:'Plants',
            tags:['products','inspirations'],
            quantity:147,
            image: require('../assets/icons/plants.png')
        },
        {
            id: 'seeds',
            name:'Seeds',
            tags:['products','shop'],
            quantity:16,
            image: require('../assets/icons/seeds.png')
        },
        {
            id: 'flowers',
            name:'Flowers',
            tags:['products','inspirations'],
            quantity:68,
            image: require('../assets/icons/flowers.png')
        },
        {
            id: 'sprayers',
            name:'Sprayers',
            tags:['products','shop'],
            quantity:17,
            image: require('../assets/icons/sprayers.png')
        },
        {
            id: 'pots',
            name:'Pots',
            tags:['products','inspirations'],
            quantity:47,
            image: require('../assets/icons/pots.png')
        },
        {
            id: 'fertilizers',
            name:'Fertilizersnts',
            tags:['products','inspirations'],
            quantity:9,
            image: require('../assets/icons/fertilizers.png')
        },

]

export const products = [
    {   
        id:1,
        name:'16 best plants that thrive in your bedroom',
        description: 'Bedrooms deserve to be decorated with lush greenery just  like every',
        tags:['Interior','27 m', 'Ideas'],
        gallery:[
            require('../assets/images/plants_1.png'),
            require('../assets/images/plants_1.png'),
            require('../assets/images/plants_1.png'),
            require('../assets/images/plants_1.png'),
            require('../assets/images/plants_1.png'),
            require('../assets/images/plants_1.png'),
        ]
    }
]

export const explore = [
    require('../assets/images/explore_1.png'),
    require('../assets/images/explore_2.png'),
    require('../assets/images/explore_3.png'),
    require('../assets/images/explore_4.png'),
    require('../assets/images/explore_5.png'),
    require('../assets/images/explore_6.png'),
]

export const profile = {
    username: 'mikamokedo',
    location: 'Viet Nam',
    email: 'mikamokedo@gmail.com',
    avatar: require('../assets/images/avatar.png'),
    budget: 1000,
    monthly_cap: 5000,
    notifications: true,
    newsletter: false,

}