import React, { useState } from "react";
import Slider from "@react-native-community/slider";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  Dimensions,
  Switch,
} from "react-native";
import { colors } from "../constants/theme";
import { profile } from "../constants/data";
const { width, height } = Dimensions.get("window");

export default function SettingsTab() {
  const [userName, setUserName] = useState(profile.username);
  const [location, setLocation] = useState(profile.location);
  const [onEditUserName, setonEditUserName] = useState(false);
  const [onEditLocation, setOnEditLocation] = useState(false);
  const [budget, setBudget] = useState(profile.budget);
  const [money, setMoney] = useState(profile.monthly_cap);
  const [noti, setnoti] = useState(false);
  const [news, setNews] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Settings</Text>
        <Image
          source={require("../assets/images/avatar.png")}
          style={styles.avatar}
        />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        style={{ marginTop: 30 }}
      >
        <View style={{ marginBottom: 10 }}>
          <Text style={styles.label}>User Name</Text>
          <View style={styles.boxEdit}>
            {onEditUserName ? (
              <TextInput
                value={userName}
                onChangeText={(value) => setUserName(value)}
              />
            ) : (
              <Text style={styles.mainText}>{userName}</Text>
            )}
            <Text
              style={styles.edit}
              onPress={() => setonEditUserName(!onEditUserName)}
            >
              Edit
            </Text>
          </View>
        </View>
        <View style={{ marginBottom: 5 }}>
          <Text style={styles.label}>Location</Text>
          <View style={styles.boxEdit}>
            {onEditLocation ? (
              <TextInput
                value={location}
                onChangeText={(value) => setLocation(value)}
              />
            ) : (
              <Text style={styles.mainText}>{location}</Text>
            )}

            <Text style={styles.edit}>Edit</Text>
          </View>
        </View>
        <View style={{ marginBottom: 20 }}>
          <Text style={styles.label}>E-mail</Text>
          <View style={styles.boxEdit}>
            <Text style={styles.mainText}>{profile.email}</Text>
          </View>
        </View>
        <View style={{ marginBottom: 20 }}>
          <Text style={styles.label}>Budget</Text>
          <Slider
            minimumValue={0}
            maximumValue={2000}
            value={budget}
            minimumTrackTintColor={colors.primary}
            maximumTrackTintColor="#000000"
            onValueChange={(value) => setBudget(value)}
            thumbImage={require("../assets/icons/fertilizers.png")}
          />
          <Text style={{ ...styles.label, textAlign: "right", marginTop: 10 }}>
            ${budget}
          </Text>
        </View>
        <View style={{ marginBottom: 20 }}>
          <Text style={styles.label}>Monthly cap</Text>
          <Slider
            minimumValue={0}
            maximumValue={8000}
            value={money}
            minimumTrackTintColor={colors.primary}
            maximumTrackTintColor="#000000"
            onValueChange={(value) => setMoney(value)}
            thumbImage={require("../assets/icons/plants.png")}
          />
          <Text style={{ ...styles.label, textAlign: "right", marginTop: 10 }}>
            ${money}
          </Text>
        </View>
        <View
          style={{
            marginBottom: 20,
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text style={styles.label}>Notification</Text>
          <Switch value={noti} onValueChange={(value) => setnoti(value)} />
        </View>
        <View
          style={{
            marginBottom: 20,
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text style={styles.label}>Newsletter</Text>
          <Switch value={news} onValueChange={(value) => setNews(value)} />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  avatar: {
    width: 50,
    height: 50,
  },
  container: {
    paddingHorizontal: 20,
    flex: 1,
    marginTop: 50,
  },
  label: {
    color: colors.gray2,
    marginBottom: 5,
  },
  boxEdit: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  edit: {
    fontWeight: "bold",
    color: colors.primary,
  },
  mainText: {
    fontWeight: "bold",
    fontSize: 16,
  },
});
