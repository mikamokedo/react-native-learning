import React from "react";
import { Formik } from "formik";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import * as Yup from "yup";
import { colors } from "../constants/theme";
import ButtonRadian from "../components/ButtonRadian";
import Icon from "react-native-vector-icons/FontAwesome";
import AsyncStorage from "@react-native-async-storage/async-storage";

const SignupSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  password: Yup.string()
    .min(6, "Too Short!")
    .max(10, "Too Long!")
    .required("Required"),
  user: Yup.string()
    .min(6, "Too Short!")
    .max(10, "Too Long!")
    .required("Required"),
});
type Props = {
  navigation: any;
};
export default function SignUp({ navigation }: Props) {
  const [secure, setSecure] = React.useState(false);
  const onSubmitSigup = async (values: {
    email: string;
    password: string;
    user: string;
  }) => {
    try {
      const current = await AsyncStorage.getItem("user");
      if (current === null) {
        const temp = JSON.stringify([values]);
        await AsyncStorage.setItem("user", temp);
        navigation.navigate("login");
      } else {
        let data = JSON.parse(current);
        data.push(values);
        await AsyncStorage.setItem("user", JSON.stringify(data));
        navigation.navigate("login");
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <KeyboardAvoidingView style={{ flex: 1 }}>
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <Text style={styles.title}>Sign Up</Text>
        </View>

        <Formik
          initialValues={{ email: "", password: "", user: "" }}
          onSubmit={(values) => onSubmitSigup(values)}
          validationSchema={SignupSchema}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            errors,
            touched,
            values,
          }) => (
            <View style={styles.boxCenter}>
              <View style={styles.inputbox}>
                <Text style={styles.label}>Email</Text>
                <TextInput
                  onChangeText={handleChange("email")}
                  placeholder="type your email"
                  style={styles.textInput}
                  onBlur={handleBlur("email")}
                  value={values.email}
                  keyboardType="email-address"
                />
                {errors.email && touched.email ? (
                  <Text style={{ color: "red" }}>{errors.email}</Text>
                ) : null}
              </View>
              <View style={styles.inputbox}>
                <Text style={styles.label}>User</Text>
                <TextInput
                  onChangeText={handleChange("user")}
                  placeholder="type your user"
                  style={styles.textInput}
                  onBlur={handleBlur("user")}
                  value={values.user}
                />
                {errors.user && touched.user ? (
                  <Text style={{ color: "red" }}>{errors.user}</Text>
                ) : null}
              </View>
              <View style={styles.inputbox}>
                <Text style={styles.label}>Password</Text>

                <TextInput
                  onChangeText={handleChange("password")}
                  placeholder="type your password"
                  secureTextEntry={!secure}
                  style={styles.textInput}
                  onBlur={handleBlur("password")}
                  value={values.password}
                />
                <Icon
                  style={styles.eysButton}
                  name={secure ? "eye" : "eye-slash"}
                  size={20}
                  color="gray"
                  onPress={() => setSecure(!secure)}
                />
                {errors.password && touched.password ? (
                  <Text style={{ color: "red" }}>{errors.password}</Text>
                ) : null}
              </View>
              <View style={styles.boxButton}>
                <ButtonRadian name="Signup" onPress={handleSubmit} />
              </View>
              <Text
                style={{
                  ...styles.label,
                  textAlign: "center",
                  marginTop: 20,
                  textDecorationLine: "underline",
                }}
                onPress={() => navigation.navigate("login")}
              >
                You have an account already?
              </Text>
            </View>
          )}
        </Formik>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    flex: 1,
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  label: {
    color: colors.gray,
  },
  inputbox: {
    marginBottom: 20,
  },
  textInput: {
    borderColor: "transparent",
    borderBottomColor: colors.gray2,
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  boxButton: {
    marginTop: 30,
  },
  eysButton: {
    position: "absolute",
    right: 10,
    top: 30,
  },
  boxCenter: {
    flex: 3,
  },
});
