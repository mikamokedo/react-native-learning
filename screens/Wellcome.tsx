import React, { useState } from "react";
import { colors } from "../constants/theme";
import Swiper from "react-native-swiper";
import { LinearGradient } from "expo-linear-gradient";
import ButtonRadian from "../components/ButtonRadian";
import { StackNavigationProp } from "@react-navigation/stack";

import {
  FlatList,
  StyleSheet,
  Text,
  View,
  Image,
  ImageProps,
  Dimensions,
  TouchableOpacity,
  Modal,
  ScrollView,
} from "react-native";

const { width, height } = Dimensions.get("window");
type typeItem = {
  item: ImageProps;
};

type Props = {
  navigation: any;
};
export default function Wellcome({ navigation }: Props) {
  const [modalTerm, setModalTerm] = useState(false);
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>
          You Home. <Text style={styles.green}>Greener.</Text>
        </Text>
        <Text style={styles.titleSmall}>Enjoy the experience.</Text>
      </View>
      <View style={styles.containerSlide}>
        <Swiper
          autoplay
          loop
          autoplayTimeout={5}
          dot={
            <View
              style={{
                backgroundColor: "#fff",
                width: 13,
                height: 13,
                borderRadius: 7,
                marginLeft: 7,
                marginRight: 7,
              }}
            />
          }
          activeDot={
            <View
              style={{
                backgroundColor: colors.secondary,
                width: 13,
                height: 13,
                borderRadius: 7,
                marginLeft: 7,
                marginRight: 7,
              }}
            />
          }
          paginationStyle={{}}
        >
          <View style={styles.slide}>
            <Image
              style={{ width, height: height / 2 }}
              source={require("../assets/images/illustration_1.png")}
              resizeMode="cover"
            />
          </View>
          <View style={styles.slide}>
            <Image
              style={{ width, height: height / 2 }}
              source={require("../assets/images/illustration_2.png")}
              resizeMode="cover"
            />
          </View>
          <View style={styles.slide}>
            <Image
              style={{ width, height: height / 2 }}
              source={require("../assets/images/illustration_3.png")}
            />
          </View>
        </Swiper>
      </View>
      <View style={styles.wrapTwoButton}>
        <ButtonRadian
          name="Login"
          onPress={() => navigation.navigate("login")}
        />
        <TouchableOpacity
          style={{ ...styles.button, marginTop: 10 }}
          activeOpacity={0.8}
          onPress={() => navigation.navigate("signup")}
        >
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            locations={[0.1, 0.9]}
            style={styles.button}
            colors={[colors.gray, colors.gray2]}
          >
            <Text style={styles.textwhite}>SignUp</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <Text style={styles.textServices} onPress={() => setModalTerm(true)}>
        Team of services
      </Text>
      <Modal animationType="slide" transparent={true} visible={modalTerm}>
        <View style={styles.centeredView}>
          <Text style={styles.termsTitle}>Terms of Services</Text>
          <View style={styles.textTerm}>
            <ScrollView>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
              <Text>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Exercitationem, recusandae repudiandae voluptate fugiat ipsa
                aperiam voluptatibus reprehenderit illo, tempore, natus commodi
                dolore a iste in doloremque consequatur soluta magni aspernatur.
              </Text>
            </ScrollView>
          </View>
          <View style={{ flex: 1, marginTop: 10, justifyContent: "center" }}>
            <ButtonRadian
              name="I Understand"
              onPress={() => setModalTerm(false)}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 25,
    marginTop: 40,
  },
  green: {
    color: colors.primary,
  },
  titleSmall: {
    color: colors.gray2,
    textAlign: "center",
    fontSize: 16,
  },
  slide: {
    flex: 1,
    backgroundColor: "transparent",
  },
  container: {
    flex: 1,
    justifyContent: "space-around",
    backgroundColor: "#fff",
  },
  button: {
    borderRadius: 5,
    height: 40,
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 2,
  },
  textwhite: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
  },
  wrapTwoButton: {
    paddingHorizontal: 20,
  },
  containerSlide: {
    height: height / 2,
  },
  textServices: {
    textAlign: "center",
    color: colors.gray2,
  },
  centeredView: {
    flex: 1,
    padding: 20,
    alignItems: "stretch",
    backgroundColor: "white",
  },
  termsTitle: {
    fontSize: 20,
    color: colors.gray2,
    flex: 1,
  },
  textTerm: {
    flex: 8,
  },
});
