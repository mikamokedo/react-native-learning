import React from "react";
import { Formik } from "formik";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import * as Yup from "yup";
import { colors } from "../constants/theme";
import ButtonRadian from "../components/ButtonRadian";
import Icon from "react-native-vector-icons/FontAwesome";
import AsyncStorage from "@react-native-async-storage/async-storage";

const SignupSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  password: Yup.string()
    .min(6, "Too Short!")
    .max(10, "Too Long!")
    .required("Required"),
});

const forGotSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
});

type Props = {
  navigation: any;
};
export default function Login({ navigation }: Props) {
  const [secure, setSecure] = React.useState(false);
  const [onForgotPass, setOnForgotPass] = React.useState(false);
  const onSubmitLogin = async (values: { email: string; password: string }) => {
    try {
      const current = await AsyncStorage.getItem("user");
      if (current !== null) {
        let temp = JSON.parse(current);
        const check = temp.find(
          (item: { email: string; password: string; user: string }) =>
            item.email === values.email && item.password === values.password
        );
        if (check) {
          navigation.navigate("browse");
        } else {
          console.log("false");
        }
      } else {
      }
    } catch (error) {
      console.log(error);
    }
  };
  const onSubmitForgot = (values: { email: string }) => {
    console.log(values);
  };
  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="height">
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <Text style={styles.title}>Login</Text>
        </View>
        {onForgotPass ? (
          <Formik
            initialValues={{ email: "" }}
            onSubmit={(values) => onSubmitForgot(values)}
            validationSchema={forGotSchema}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              errors,
              touched,
              values,
            }) => (
              <View style={styles.boxCenter}>
                <View style={styles.inputbox}>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    onChangeText={handleChange("email")}
                    placeholder="type your email"
                    style={styles.textInput}
                    onBlur={handleBlur("email")}
                    value={values.email}
                    keyboardType="email-address"
                  />
                  {errors.email && touched.email ? (
                    <Text style={{ color: "red" }}>{errors.email}</Text>
                  ) : null}
                </View>
                <View style={styles.boxButton}>
                  <ButtonRadian name="Submit" onPress={handleSubmit} />
                </View>
              </View>
            )}
          </Formik>
        ) : (
          <Formik
            initialValues={{ email: "", password: "" }}
            onSubmit={(values) => onSubmitLogin(values)}
            validationSchema={SignupSchema}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              errors,
              touched,
              values,
            }) => (
              <View style={styles.boxCenter}>
                <View style={styles.inputbox}>
                  <Text style={styles.label}>Email</Text>
                  <TextInput
                    onChangeText={handleChange("email")}
                    placeholder="type your email"
                    style={styles.textInput}
                    onBlur={handleBlur("email")}
                    value={values.email}
                    keyboardType="email-address"
                  />
                  {errors.email && touched.email ? (
                    <Text style={{ color: "red" }}>{errors.email}</Text>
                  ) : null}
                </View>
                <View style={styles.inputbox}>
                  <Text style={styles.label}>Password</Text>

                  <TextInput
                    onChangeText={handleChange("password")}
                    placeholder="type your password"
                    secureTextEntry={!secure}
                    style={styles.textInput}
                    onBlur={handleBlur("password")}
                    value={values.password}
                  />
                  <Icon
                    style={styles.eysButton}
                    name={secure ? "eye" : "eye-slash"}
                    size={20}
                    color="gray"
                    onPress={() => setSecure(!secure)}
                  />
                  {errors.password && touched.password ? (
                    <Text style={{ color: "red" }}>{errors.password}</Text>
                  ) : null}
                </View>
                <View style={styles.boxButton}>
                  <ButtonRadian name="Login" onPress={handleSubmit} />
                </View>
                <Text
                  style={{
                    ...styles.label,
                    textAlign: "center",
                    marginTop: 20,
                    textDecorationLine: "underline",
                  }}
                  onPress={() => setOnForgotPass(true)}
                >
                  Forgot your password?
                </Text>
              </View>
            )}
          </Formik>
        )}
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    flex: 1,
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  label: {
    color: colors.gray,
  },
  inputbox: {
    marginBottom: 20,
  },
  textInput: {
    borderColor: "transparent",
    borderBottomColor: colors.gray2,
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  boxButton: {
    marginTop: 30,
  },
  eysButton: {
    position: "absolute",
    right: 10,
    top: 30,
  },
  boxCenter: {
    flex: 3,
  },
});
