import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageProps,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { colors } from "../constants/theme";
import { categories } from "../constants/data";
const { width, height } = Dimensions.get("window");
type Props = {
  navigation: any;
};

export default function Browse({ navigation }: Props) {
  const tabs = [
    { key: "products", title: "Products" },
    { key: "inspirations", title: "Inspirations" },
    { key: "shop", title: "Shop" },
  ];
  const [activeTab, setActiveTab] = useState<string>("products");
  const [products, setProducts] = useState<
    {
      id: string;
      name: string;
      tags: string[];
      quantity: number;
      image: ImageProps;
    }[]
  >([]);
  useEffect(() => {
    const temp = categories.filter(
      (item) => !item.tags.every((tag) => tag !== activeTab)
    );
    setProducts(temp);
  }, [activeTab]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Browse</Text>
        <TouchableOpacity onPress={() => navigation.navigate("settings")}>
          <Image
            source={require("../assets/images/avatar.png")}
            style={styles.avatar}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.tabs}>
        {tabs.map((item) => (
          <Text
            style={{
              ...styles.titleTabs,
              color: activeTab === item.key ? colors.primary : colors.black,
              borderBottomWidth: 2,
              borderBottomColor:
                activeTab === item.key ? colors.primary : "transparent",
            }}
            onPress={() => setActiveTab(item.key)}
            key={item.key}
          >
            {item.title}
          </Text>
        ))}
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.wrapList}>
          {products.map(
            (item: {
              id: string;
              name: string;
              tags: string[];
              quantity: number;
              image: ImageProps;
            }) => (
              <View style={styles.wrapProduct} key={item.id}>
                <View style={styles.wrapImages}>
                  <Image source={item.image} />
                </View>

                <Text style={styles.nameProduct}>{item.name}</Text>
                <Text style={styles.quantityProduct}>
                  {item.quantity} Products
                </Text>
              </View>
            )
          )}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    flex: 1,
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  avatar: {
    width: 50,
    height: 50,
  },
  tabs: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomColor: colors.gray2,
    borderBottomWidth: 1,
  },
  titleTabs: {
    fontSize: 18,
    fontWeight: "bold",
    paddingVertical: 5,
  },
  wrapList: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
  },
  wrapProduct: {
    width: (width - 60) / 2,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    marginBottom: 20,
    paddingVertical: 20,
    borderRadius: 5,
  },
  wrapImages: {
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#CDF6E7",
  },
  nameProduct: {
    fontWeight: "bold",
    fontSize: 16,
    marginTop: 10,
  },
  quantityProduct: {
    color: colors.gray2,
  },
});
